// Importacion de modulos de la aplicacion 
const express = require('express');
const app = express();
const morgan = require('morgan')
const session = require('express-session');

// Confiuraciones 
app.set('port', process.env.PORT || 3000)

// Middlewares 
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// Routes
app.use(require('./routes/usuarios')); // Ruta del modulo de Usuarios 
app.use(require('./routes/asignaturas')); // Ruta del modulo de asignaturas
app.use(require('./routes/matricula')); // Ruta del modulo de matriculas
app.use(require('./routes/estudiantes')); // Ruta del modulo del Docente

// Iniciando el servidor
app.listen(app.get('port'), () => {
    console.log('Server on port ', app.get('port'));
});