const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

// Metodo para consultar todos los estudiantes
router.get('/estudiantes', (req, res) => {
    mysqlConnection.query('SELECT b.id idUser, a.id idAU, b.cedula, b.nombres, b.apellidos, b.email, c.descripcion, c.creditos, c.id, a.calificacion FROM usuario_asignatura a INNER JOIN usuarios b ON a.id_usuario = b.id INNER JOIN asignaturas c ON a.id_asignatura = c.id WHERE b.rol_id IN(3)', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

// Metodo que permite actualizar la calificacion del estudiante
router.put('/estudiantes', (req, res) => {
    console.log('UPDATE usuario_asignatura SET calificacion ="' + req.body[0] + '" WHERE id="' + req.body[1] + '"')
    mysqlConnection.query('UPDATE usuario_asignatura SET calificacion ="' + req.body[0] + '" WHERE id="' + req.body[1] + '"', (err, rows, fields) => {
        if (!err) {
            res.json({ code: 200, status: 'Usuario Actualizado Correctamente!' });
        } else {
            console.log(err);
        }
    });
});

module.exports = router;