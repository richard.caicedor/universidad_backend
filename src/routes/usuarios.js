const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

// Metodo para consultar todos los usuarios de la base de datos 
router.get('/usuarios', (req, res) => {
    mysqlConnection.query('SELECT a.*, b.descripcion FROM usuarios a INNER JOIN roles b ON a.rol_id = b.id ORDER BY id DESC', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});


// Metodo para almacenar la informacion de los usuarios en la base de datos
router.post('/usuarios', (req, res) => {
    var { id, cedula, nombres, apellidos, email, celular, rol_id, password } = req.body;
    mysqlConnection.query('SELECT * FROM usuarios WHERE cedula = "' + cedula + '"', (err, rows, fields) => {
        if (rows[0]) {
            res.json({ code: 401, status: 'Error!! El usuarios ya existe' });
        } else {
            var query = ' INSERT INTO usuarios VALUES (null,?,?,?,?,?,?,MD5(?))';
            mysqlConnection.query(query, [cedula, nombres, apellidos, email, celular, rol_id, password], (err, rows, fields) => {
                if (!err) {
                    res.json({ code: 200, status: 'Usuario Agregado Exitosamente!' });
                } else {
                    console.log(err);
                }
            });
        }
    });
});

// Metodo para eliminar la informacion de los usuarios
router.delete('/usuarios/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('DELETE FROM usuarios WHERE id=?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ code: 200, status: 'Usuario Eliminado' });
        } else {
            console.log(err);
        }
    });
});

// Metodo que permite obtener la informacion de un usuario especifico 
router.post('/login', (req, res) => {
    const { cedula, password } = req.body;
    mysqlConnection.query('SELECT * FROM usuarios WHERE cedula = "' + cedula + '" AND password = MD5("' + password + '")', (err, rows, fields) => {
        if (rows[0]) {
            res.json({ status: 200, data: rows[0] });
        } else {
            res.json({ status: 401, data: 'Error' });
        }
    });
});

module.exports = router;