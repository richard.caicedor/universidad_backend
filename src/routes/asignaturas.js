const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');

// Metodo para consultar todos las asignaturas de la base de datos 
router.get('/asignatura', (req, res) => {
    mysqlConnection.query('SELECT a.* FROM asignaturas a ORDER BY a.id DESC', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

// Metodo para consultar todas las asignaturas de un usuario determinado 
router.get('/asignatura/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('SELECT a.* , b.id idUA, b.calificacion , b.creditos creditosUA FROM asignaturas a LEFT JOIN usuario_asignatura b ON a.id = b.id_asignatura AND b.id_usuario = ' + id + ' WHERE b.calificacion =0 OR b.calificacion IS NULL ORDER BY a.id DESC', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});



// Metodo para almacenar la informacion de las asignaturas en la base de datos
router.post('/asignatura', (req, res) => {
    var { id, descripcion, observacion, creditos } = req.body;
    var query = ' INSERT INTO asignaturas VALUES (null,?,?,?)';
    mysqlConnection.query(query, [descripcion, observacion, creditos], (err, rows, fields) => {
        if (!err) {
            res.json({ code: 200, status: 'Asignatura Agregada Exitosamente!' });
        } else {
            console.log(err);
        }
    });
});

// Metodo para eliminar la informacion de una asignatura
router.delete('/asignatura/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('DELETE FROM asignaturas WHERE id=?', [id], (err, rows, fields) => {
        if (!err) {
            res.json({ code: 200, status: 'Registro Eliminado' });
        } else {
            console.log(err);
        }
    });
})

module.exports = router;