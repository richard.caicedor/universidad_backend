const express = require('express');
const router = express.Router();

const mysqlConnection = require('../database');


// Metodo para almacenar la informacion de la matricula
router.post('/matricula/:id', (req, res) => {
    const { id } = req.params;
    var array = [];
    var query = 'INSERT INTO usuario_asignatura VALUES ';
    req.body.forEach(function(element) {
        array = element.split('-');
        query += ' (null,' + id + ',' + array[0] + ',' + array[1] + ',0), ';
    });
    query = query.slice(0, -2);
    mysqlConnection.query(query, (err, rows, fields) => {
        if (!err) {
            res.json({ code: 200, status: 'Registro Insertado Exitosamente!' });
        } else {
            console.log(err);
        }
    });
});

// Metodo que permite obtener las calificaciones de los alumnos
router.get('/calificacion/:id', (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('SELECT a.* , b.id idUA, b.calificacion, b.creditos creditosUA FROM asignaturas a INNER JOIN usuario_asignatura b ON a.id = b.id_asignatura  WHERE  b.id_usuario = ' + id + ' ORDER BY a.id DESC', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

module.exports = router;